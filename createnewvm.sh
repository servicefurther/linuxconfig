#New-VM -Name dockerhost2 -path d:\vm\dockerhost2\ --MemoryStartupBytes 512MB
#New-VHD -Path d:\vm\dockerhost2\dockerhost2.vhdx -SizeBytes 4GB -Dynamic

$newVMName = "dockerhost_" + (Get-Random 10)
New-Item -ItemType directory -Path "D:\VM\$newVMName" | Out-Null
Import-VM -Path "D:\mastercopy\dockerhost1\Virtual Machines\C1D9413F-F59D-4A49-970E-AF8B2FB8AB68.vmcx" -Copy -GenerateNewId -SmartPagingFilePath "D:\VM\$newVMName" -SnapshotFilePath "D:\VM\$newVMName" -VhdDestinationPath "D:\VM\$newVMName\VHD" -VirtualMachinePath "D:\VM\$newVMName"
Rename-VM "dockerhost1" -NewName $newVMName
Start-VM $newVMName
$Adapters=($newVMName | Get-VMNetworkAdapter)
ForEach ($Adapter in $Adapters){Write-Host($VM.VMName,’ ‘, $Adapter.Name, ‘ ‘, $Adapter.IPAddresses[0])}
#ssh root@ip
#echo $newVMName > /etc/hostname $newVMName

