#!/usr/bin/env sh
#OR !/bin/sh
#wget -O - https://bitbucket.org/servicefurther/linuxconfig/raw/master/config.sh | sh
#whoami
#uname -a 
#cat /etc/alpine-release 
#apk add nano sudo
#nano /etc/apk/repositories
#uncomment community
#apk update

echo ┌────────────────────────────┐
echo │ Alpine Linux Config Script │
echo └────────────────────────────┘

# Colour variables
BLACK='\033[0;30m'
RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[0;33m'
BLUE='\033[0;34m'
PURPLE='\033[0;35m'
CYAN='\033[0;36m'
GREY='\033[0;37m'
DARKGREY='\033[1;30m'
BRIGHTRED='\033[1;31m'
BRIGHTGREEN='\033[1;32m'
BRIGHTYELLOW='\033[1;33m'
BRIGHTBLUE='\033[1;34m'
BRIGHTPURPLE='\033[1;35m'
BRIGHTCYAN='\033[1;36m'
WHITE='\033[1;37m'
RESET='\033[0m'


echo -e "${BRIGHTGREEN}Update package list${RESET}"
apk update


echo -e "${BRIGHTGREEN}Installing Hyper-V modules${RESET}"
apk add hvtools
rc-service hv_fcopy_daemon start
rc-service hv_kvp_daemon start
rc-service hv_vss_daemon start
rc-update add hv_fcopy_daemon
rc-update add hv_kvp_daemon
rc-update add hv_vss_daemon

echo -e "${BRIGHTGREEN}Configuring SSH Daemon${RESET}"
if [ ! -f "/etc/ssh/sshd_config.ORIGINAL" ]; then
	echo -e "${GREEN}Backing up config file${RESET}"
	cp /etc/ssh/sshd_config /etc/ssh/sshd_config.ORIGINAL
fi
sed -i 's:#PermitRootLogin prohibit-password:PermitRootLogin yes:' /etc/ssh/sshd_config
rc-service sshd restart
setup-xorg-base xfce4 xfce4-terminal dbus-x11 thunar-volman
rc-service dbus start
rc-update add dbus
rc-service udev start
rc-update add udev
apk add lightdm lightdm-gtk-greeter
rc-update add lightdm
apk add polkit consolekit2
#apk add firefox filezilla leafpad faenza-icon-theme
apk search vmmouse 
apk search vmware
apk add xf86-input-vmmouse xf86-video-vmware
apk add open-vm-tools-gtk
rc-update add open-vm-tools
apk add --update docker
rc-update add docker boot
apk add curl
service docker start
echo -e "${BRIGHTGREEN}Done!${RESET}"